"use strict";
//import * as jQuery__ from "jquery";
/**\ The Typescript playground fails immediately here, reporting:
 **     "Synchronous require cannot resolve module 'jquery'."
 **     .."This is the first mention of this module!"
 ** NB: module 'jquery' (ie not 'jQu...' or any such)
 ** Whilst suggestive, there's no point fixing this yet because, if it
 ** ran, the code below would proceed to destroy the playground(!)
\**/
const veracity = true;
veracity; // Define logical contingent constants (sic) for use in scaffolding
const falsity = false;
falsity; // They are useful for suppressing 'unreachable code' warnings
const to_head = (" " // HTML fragment to append to 'head' element
    + "        <!-- Bootstrap CSS -->"
    + "        <link rel=\"stylesheet\" crossorigin=\"anonymous\""
    + "href=\"https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css\""
    + "integrity=\"sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm\" />"
    + "        <title>Bootstrapping-bootstrap demo!</title>"
    + "        <style type=\"text/css\">"
    + "            .row div {"
    + "                border: 1px solid silver;"
    + "            }"
    + "        </style>"
    + ""
    + "" // The following <script> line probably won't wark but, hey, you can try it in _your_ browser:-
    + ""
/* +"<script>console.log(\"Loading jQuery, popper, and bootstrap.\");</script>" /**/
);
const to_body = (" " // HTML fragment to append to 'head' element.  First, Neil's table:-
    + ""
    + "        <p>This example by Neil Rowe (src: &lt;https://scrimba.com/learn/bootstrap4&gt;):-</p>"
    + "        <div class=\"container\" style=\"margin-top:30px;\">"
    + "            <div class=\"row\">"
    + "                <div class=\"col-sm-6 col-md-3\">column</div>"
    + "                <div class=\"col-sm-6 col-md-3\">column</div>"
    + "                <div class=\"col-sm-6 col-md-3\">column</div>"
    + "                <div class=\"col-sm-6 col-md-3\">column</div>"
    + "            </div>"
    + "        </div>"
    + ""
    + "" // Now for the dicey bit.  This commented-out HTML section doesn't (portably) work:-
    + "" /*
+"        <!-- Optional JavaScript -->"
+"        <!-- jQuery first, then Popper.js, then Bootstrap JS -->"
+"        <script crossorigin=\"anonymous\""
+"src=\"https://code.jquery.com/jquery-3.2.1.slim.min.js\""
+"integrity=\"sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN\"></script>"
+"        <script crossorigin=\"anonymous\""
+"src=\"https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js\""
+"integrity=\"sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q\"></script>"
+"        <script crossorigin=\"anonymous\""
+"src=\"https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js\""
+"integrity=\"sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl\"></script>"
+"" /**/
    + ""
    + "" // This shameful technique achieves the same effect without (sadly) the integrity check...
);
const str2frag = function (htxt) {
    const frag = document.createDocumentFragment();
    const elem = document.createElement('html'); // The element-type isn't crucial anyway
    elem.innerHTML = htxt;
    for (let i = elem.childNodes.length; --i >= 0;) {
        frag.prepend(elem.childNodes[i]);
    }
    return frag;
};
const also_to_body = function (to_load, to_exec, so_far, index = 0 - 1) {
    const tl = to_load[++index];
    if (tl) { }
    else {
        document.body.append(str2frag(so_far + " " + to_exec + "' />"));
        return;
    }
    fetch(tl)
        .then((got) => got.text())
        .then((txt) => {
        also_to_body(to_load, to_exec, (so_far ? so_far : "<img hidden=true src='http://127.0.0.1/devnull.gif' onerror='") + txt.replace(/'/g, "&apos;"), index);
    });
};
var once = 2;
const ignite = function () {
    console.log("Document is " + document.readyState); // Remove this line later, obviously
    if ((once >>= 1) === 0) {
        console.log("We ignited again, but who cares?");
    } // make idempotent
    else {
        console.log("We're igniting!"); // Remove this line later, obviously
        if (falsity) { // The blunt-force trauma method:-
            const new_head = document.head.innerHTML + to_head;
            const new_body = document.body.innerHTML + to_body;
            document.head.innerHTML = new_head;
            document.body.innerHTML = new_body;
        }
        else if (veracity) { // The subtle-knife mothod:-
            document.head.append(str2frag(to_head));
            document.body.append(str2frag(to_body));
        }
        else { }
        also_to_body([
            "https://code.jquery.com/jquery-3.2.1.slim.min.js",
            "https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js",
            //"https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js", // BUG: Throws a TypeError
        ], "console.log(\"Loaded jQuery V\" + jQuery(document).jquery + \", popper, and bootstrap.\");");
        console.log("We ignited!"); // Remove this line later, obviously
    }
};
function readiness() { return 1 << document.readyState.length; } // use length as hash
const unready = 1 << "uninitialized".length | 1 << "loading".length; // unreadiness vector
document.addEventListener('readystatechange', event => {
    if (!event || readiness() & unready) { }
    else
        ignite();
});
if (readiness() & unready) { }
else
    ignite();

